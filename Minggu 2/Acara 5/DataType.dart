//DATATYPE

void main() {
//hello world
  print("Hello World"); //Hello World

//string
  var sentences = "widha";
  print(sentences[0]); // "w"
  print(sentences[2]); // "d"

//number
  // declare an integer
  int num1 = 10;

  // declare a double value
  double num2 = 10.50;
  // print the values

  print(num1); //10
  print(num2); //10.5

//integer 1
  print(num.parse('12')); //12
  print(num.parse('10.91')); //10.91

//integer 2
  print(num.parse('12A'));
  print(num.parse('AAAA')); //error

//integer to string
  int j = 45;
  String t = "$j";
  print("hello" + t);
}
