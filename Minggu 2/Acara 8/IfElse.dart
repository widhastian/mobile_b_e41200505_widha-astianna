import 'dart:io';

main() {
  String? nama, peran;

  stdout.write('Masukan nama kamu: ');
  nama = stdin.readLineSync()!;
  stdout.write('Masukan peran kamu: ');
  peran = stdin.readLineSync();
  if (nama == null && peran == null) {
    print('masukan nama dan peran');
  } else {
    if (nama == 'Widha' && peran == 'Penyihir') {
      print("Selamat datang di Dunia Werewolf, Widha");
      print(
          "Halo Penyihir Widha, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (nama == 'Asti' && peran == 'Guard') {
      print("Selamat datang di Dunia Werewolf, Asti");
      print("Halo Guard Asti, kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (nama == 'Anna' && peran == 'Werewolf') {
      print("Selamat datang di Dunia Werewolf, Anna");
      print(
          "Halo Werewolf Anna, kamu dapat melihat siapa yang menjadi werewolf!");
    } else {
      print("Selamat datang di Dunia Werewolf, $nama");
      print(
          "Halo $peran $nama, kamu dapat melihat siapa yang menjadi werewolf!");
    }
  }
}
