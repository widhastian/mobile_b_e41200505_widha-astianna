import 'package:flutter/material.dart';

import 'package:flutter_application_2/router/routes.dart';

import 'package:flutter_application_2/scaffold/sca1.dart';
import 'package:flutter_application_2/scaffold/sca2.dart';
import 'package:flutter_application_2/scaffold/sca3.dart';
import 'package:flutter_application_2/scaffold/sca4.dart';
import 'package:flutter_application_2/scaffold/sca5.dart';

import 'package:flutter_application_2/NavBottom/home.dart';

import 'navigation.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // int index = 0;
  // List<Widget> list = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
//NAVIGATION BOTTOM
      onGenerateRoute: RouteGenerator.generateRoute,

      // home: Home(),

// ROUTING
      debugShowCheckedModeBanner: false,
      // routes: {
      //   // '/': (context) => sca1(),
      //   // '/': (context) => sca2(),
      //   // '/': (context) => sca3(),
      //   // '/': (context) => sca4(),
      //   // '/': (context) => sca5(),
      //   // '/': (context) => navigation(),
      //   // '/': (context) => Navigator(),
      // },

// DRAWER WIDGET
      // home: Scaffold(
      //   appBar: AppBar(
      //     title: Text("Navigation Drawer"),
      //   ),
      //   body: list[index],
      // ),
    );
  }
}
