import 'package:flutter/material.dart';

void main() => runApp(BelajarImage2());

class BelajarImage2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text("belajarFlutter.com"),
      ),
      body: Image.asset('assets/images/IM2.jpg'),
    ));
  }
}
