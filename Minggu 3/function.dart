void main() {
  tampilkan();
  print(munculkanangka());
  print(kalikanDua(6));
  print(kalikan(5, 6));
  tampilkanangka(5);
  print(functionPerkalian(4, 5));
}

//function sederhana tanpa return
tampilkan() {
  print("Hello Peserta Bootcamp");
}

//function sederhana dengan return
munculkanangka() {
  return 2;
}

//function dengan parameter
kalikanDua(angka) {
  return angka * 2;
}

//pengiriman parameter lebih dari satu
kalikan(x, y) {
  return x * y;
}

//inisialisasi parameter dengan nilai default
tampilkanangka(n1, {s1: 45}) {
  print(n1); //hasil akan 5 karena initialisasi 5 didalam value tampilkan
  print(s1); //hasil adalah 45 karena dari parameter diisi 45
}

//function anonymous
functionPerkalian(angka1, angka2) {
  return angka1 * angka2;
}
