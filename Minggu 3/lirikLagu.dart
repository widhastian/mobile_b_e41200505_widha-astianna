void main(List<String> args) async {
  print("Harry Styles - Fallin'");
  print("----------------------");
  await line1();
  await line2();
  await line3();
  await line4();
  await line5();
  await line6();
  await line7();
}

Future<void> line1() async {
  String message = "What am I now? What am I now?";
  final duration = Duration(seconds: 3);
  return await Future.delayed(duration, () => print(message));
}

Future<void> line2() async {
  String message = "What if I'm someone I don't want around?";
  final duration = Duration(seconds: 3);
  return await Future.delayed(duration, () => print(message));
}

Future<void> line3() async {
  String message = "I'm falling again, I'm falling again, I'm falling";
  final duration = Duration(seconds: 3);
  return await Future.delayed(duration, () => print(message));
}

Future<void> line4() async {
  String message = "What if I'm down? What if I'm out?";
  final duration = Duration(seconds: 5);
  return await Future.delayed(duration, () => print(message));
}

Future<void> line5() async {
  String message = "What if I'm someone you won't talk about?";
  final duration = Duration(seconds: 3);
  return await Future.delayed(duration, () => print(message));
}

Future<void> line6() async {
  String message = "I'm falling again, I'm falling again, I'm falling";
  final duration = Duration(seconds: 3);
  return await Future.delayed(duration, () => print(message));
}

Future<void> line7() async {
  String message = "And I get the feeling that you'll never need me again";
  final duration = Duration(seconds: 7);
  return await Future.delayed(duration, () => print(message));
}
